<?php

namespace App\DataFixtures;

use App\Entity\Post;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;


class PostFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);
            for ($i=1; $i<5; $i++)
            {
            $post = new Post();
            $post->setTitle('titre'.$i)
            ->setAuthor('author'.$i)
            ->setContent(' Les fixtures permettent de créer un jeu de données de développement
            qui pourra être partagé par toute l équipe et relancer en une
            commande (bin/console doctrine:fixtures:load)
            A l intérieur, on fera en sorte de faire des boucles pour créer des
            instances des entités dont on veut des exemples et les faire persister
           ');
           $manager->persist($post);


            }
        $manager->flush();
    }
}
