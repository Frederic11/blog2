<?php

namespace App\Controller;

use App\Entity\Post;
use App\Form\PostType;
use App\Repository\PostRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;





class PostController extends AbstractController
{
    /**
     * @Route("/post", name="post")
     */
    public function index(PostRepository $post)
    {
        return $this->render('post/index.html.twig', [
            'controller_name' => 'PostController',
            'posts'=> $post->findAll()
        ]);
    }

/**
 * @Route("/post/add", name="post_add")
 */
public function addpost(Request $request, ObjectManager $manager)
    {
        // on utilise l'entité
    $post=new Post();
    //form est le formulaire a partir de postype qui est une v
    // une variable de type clas
    $form= $this->createForm(PostType::class, $post);
    //
    $form->handleRequest($request);

    if($form->isSubmitted() && $form->isValid()) {
        $manager->persist($post);
        $manager->flush();
        return $this->redirectToRoute('post');
    }

    return $this->render('post/add.index.html.twig',[
        'form'=>$form->createView(),
    ]);
}
/**
 * @Route ("post/one/{id}", name="one_post")
 */
public function onepost(Post $post)
{
    return $this->render('post/onepost.html.twig', [
        
        'post'=> $post
    ]);
}

/**
 * @Route ("post/one/modify/{id}", name="modify-post")
 */
public function postmodif(Post $post, Request $request, ObjectManager $manager){
    $form=$this->createForm(PostType::class, $post);
    $form->handleRequest($request);

    if($form->isSubmitted() && $form->isValid()) {
        $manager->flush();}

        return $this->render('post/modifypost.html.twig',[
            'form' => $form->createView()
        ]);

}
/**
 * @Route ("post/one/resiliation/{id}", name="remove-post")
 */
public function removethepost (Post $post, Request $request, ObjectManager $manager)
{   $em=$this->getDoctrine()->getManager();
    $em->remove($post);
    $em->flush();
    return $this->render('post/remove.html.twig', [
        
        'post'=> $post
    ]);
}
}
