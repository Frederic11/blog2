<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the private '.service_locator.aoC7Sht' shared service.

return $this->privates['.service_locator.aoC7Sht'] = new \Symfony\Component\DependencyInjection\Argument\ServiceLocator($this->getService, [
    'manager' => ['services', 'doctrine.orm.default_entity_manager', 'getDoctrine_Orm_DefaultEntityManagerService.php', true],
    'post' => ['privates', '.errored..service_locator.aoC7Sht.App\\Entity\\Post', NULL, 'Cannot autowire service ".service_locator.aoC7Sht": it references class "App\\Entity\\Post" but no such service exists.'],
], [
    'manager' => '?',
    'post' => 'App\\Entity\\Post',
]);
