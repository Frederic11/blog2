<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the private '.service_locator.UdDcboK' shared service.

return $this->privates['.service_locator.UdDcboK'] = new \Symfony\Component\DependencyInjection\Argument\ServiceLocator($this->getService, [
    'App\\Controller\\PostController::addpost' => ['privates', '.service_locator.Txvqv4R', 'get_ServiceLocator_Txvqv4RService.php', true],
    'App\\Controller\\PostController::index' => ['privates', '.service_locator.CQwjxtm', 'get_ServiceLocator_CQwjxtmService.php', true],
    'App\\Controller\\PostController::onepost' => ['privates', '.service_locator.TwhmRPL', 'get_ServiceLocator_TwhmRPLService.php', true],
    'App\\Controller\\PostController::postmodif' => ['privates', '.service_locator.aoC7Sht', 'get_ServiceLocator_AoC7ShtService.php', true],
    'App\\Controller\\PostController::removethepost' => ['privates', '.service_locator.aoC7Sht', 'get_ServiceLocator_AoC7ShtService.php', true],
    'App\\Controller\\UserController::index' => ['privates', '.service_locator.V6fP8od', 'get_ServiceLocator_V6fP8odService.php', true],
    'App\\Controller\\PostController:addpost' => ['privates', '.service_locator.Txvqv4R', 'get_ServiceLocator_Txvqv4RService.php', true],
    'App\\Controller\\PostController:index' => ['privates', '.service_locator.CQwjxtm', 'get_ServiceLocator_CQwjxtmService.php', true],
    'App\\Controller\\PostController:onepost' => ['privates', '.service_locator.TwhmRPL', 'get_ServiceLocator_TwhmRPLService.php', true],
    'App\\Controller\\PostController:postmodif' => ['privates', '.service_locator.aoC7Sht', 'get_ServiceLocator_AoC7ShtService.php', true],
    'App\\Controller\\PostController:removethepost' => ['privates', '.service_locator.aoC7Sht', 'get_ServiceLocator_AoC7ShtService.php', true],
    'App\\Controller\\UserController:index' => ['privates', '.service_locator.V6fP8od', 'get_ServiceLocator_V6fP8odService.php', true],
], [
    'App\\Controller\\PostController::addpost' => '?',
    'App\\Controller\\PostController::index' => '?',
    'App\\Controller\\PostController::onepost' => '?',
    'App\\Controller\\PostController::postmodif' => '?',
    'App\\Controller\\PostController::removethepost' => '?',
    'App\\Controller\\UserController::index' => '?',
    'App\\Controller\\PostController:addpost' => '?',
    'App\\Controller\\PostController:index' => '?',
    'App\\Controller\\PostController:onepost' => '?',
    'App\\Controller\\PostController:postmodif' => '?',
    'App\\Controller\\PostController:removethepost' => '?',
    'App\\Controller\\UserController:index' => '?',
]);
